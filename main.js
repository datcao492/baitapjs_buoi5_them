const thunhapchiuthue_60 = 0.05;
const thunhapchiuthue_60_120 = 0.1;
const thunhapchiuthue_120_210 = 0.15;
const thunhapchiuthue_210_384 = 0.2;
const thunhapchiuthue_384_624 = 0.25;
const thunhapchiuthue_624_960 = 0.3;
const thunhapchiuthue_960 = 0.35;


function tinhthuethunhap() {
    var name = document.getElementById("name").value;
    var thunhapcanam = document.getElementById("num1").value * 1;
    var songuoiphuthuoc = document.getElementById("num2").value * 1;
    var thuethunhapcanhan = 0;

    var thunhapchiuthue = thunhapcanam - 4e6 - songuoiphuthuoc * 1.6e6;
    console.log(new Intl.NumberFormat().format(thunhapchiuthue));// làm đẹp số

    if (thunhapchiuthue <= 60e6){
        thuethunhapcanhan = thunhapchiuthue * thunhapchiuthue_60;
    }else if (thunhapchiuthue <= 120e6){
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + (thunhapchiuthue - 60e6) * thunhapchiuthue_60_120; 
    }else if (thunhapchiuthue <= 210e6){
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + 120e6 * thunhapchiuthue_60_120 + (thunhapchiuthue - 60e6 - 120e6) * thunhapchiuthue_120_210;
    }else if (thunhapchiuthue <= 384e6){
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + 120e6 * thunhapchiuthue_60_120 + 210e6 * thunhapchiuthue_120_210 + (thunhapchiuthue - 60e6 - 120e6 - 210e6) * thunhapchiuthue_210_384;
    } else if (thunhapchiuthue <= 624e6){
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + 120e6 * thunhapchiuthue_60_120 + 210e6 * thunhapchiuthue_120_210 + 384e6 * thunhapchiuthue_210_384 + (thunhapchiuthue - 60e6 - 120e6 - 210e6 - 384e6) * thunhapchiuthue_384_624;
    } else if (thunhapchiuthue <= 960e6){
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + 120e6 * thunhapchiuthue_60_120 + 210e6 * thunhapchiuthue_120_210 + 384e6 * thunhapchiuthue_210_384 + 634e6 * thunhapchiuthue_384_624 + (thunhapchiuthue - 60e6 - 120e6 - 210e6 - 384e6 - 634e6) * thunhapchiuthue_384_624;
    } else{
        thuethunhapcanhan = 60e6 * thunhapchiuthue_60 + 120e6 * thunhapchiuthue_60_120 + 210e6 * thunhapchiuthue_120_210 + 384e6 * thunhapchiuthue_210_384 + 634e6 * thunhapchiuthue_384_624 + 960e6 * thunhapchiuthue_624_960 + (thunhapchiuthue - 60e6 - 120e6 - 210e6 - 384e6 - 634e6 - 960e6) * thunhapchiuthue_960;
    }

    var kq = (new Intl.NumberFormat().format(thuethunhapcanhan));

    document.getElementById("result1").innerHTML = ` Họ tên: ${name} </br> Thuế thu nhập cá nhân: ${kq} `;

}